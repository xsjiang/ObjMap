#ObjMap

##Install
```
bower install ObjMap
```

##Use
```javascript
define(['ObjMap'], function(Map) {
  var m = new Map();
  m.set('name', 'xsjiang');
  console.log(m.get('name'));
});
```

##Api

###set(key, value)

###get(key)

###delete(key)

###clear()

###has(key)

###keys()

###values()

###forEach(callback)

```javascript
map.forEach(function(key, value) {

});
```

###size()