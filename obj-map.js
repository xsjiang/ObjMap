/**
 * 模拟Map。
 * @author jiangxingshang
 * @date 2016/05/17
 */

 (function(factory) {

   var root = (typeof self == 'object' && self.self === self && self) ||
             (typeof global == 'object' && global.global === global && global);
   if (typeof define === 'function' && define.amd) {
     define('ObjMap', [], function() {
       return factory();
     });

   } else if (typeof exports !== 'undefined') {
     exports.ObjMap = factory();
   } else {
     root.ObjMap = factory();
   }

 })(function() {
   var Map = function() {
     this._data = {};
   };

   Map.prototype.set = function(key, value) {
     this._data[key] = value;
     return this;
   };

   Map.prototype.get = function(key) {
     return this._data[key] || null;
   };

   Map.prototype.delete = function(key) {
     delete this._data[key];
     return this;
   };

   Map.prototype.clear = function() {
     this._data = {};
     return this;
   };

   Map.prototype.has = function(key) {
     var tmp = this._data[key];
     return tmp !== null && tmp !== undefined;
   };

   Map.prototype.keys = function() {
     var arr = [];
     for(var p in this._data) {
       if(this._data.hasOwnProperty(p)) {
         arr.push(p);
       }
     }
     return arr;
   };

   Map.prototype.values = function() {
     var arr = [];
     for(var p in this._data) {
       if(this._data.hasOwnProperty(p)) {
         arr.push(this._data[p]);
       }
     }
     return arr;
   };

   Map.prototype.forEach = function(callback) {
     for(var p in this._data) {
       if(this._data.hasOwnProperty(p)) {
         callback.call(this, p, this._data[p]);
       }
     }
   };

   Map.prototype.size = function() {
     var i = 0;
     for(var p in this._data) {
       if(this._data.hasOwnProperty(p)) {
         i++;
       }
     }
     return i;
   };

   return Map;
 });